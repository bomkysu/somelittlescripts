#include "stdio.h"
// #include "regex"
#include <iostream>
// #include <array>


using namespace std;

enum numm 
{
    mot,
    hai,
    ba
};

namespace vfc
{
    typedef unsigned int uint32_t;
} // namespace name


typedef union
{
    struct St_Fids
    {
        vfc::uint32_t m_FID_Ubat                   : 1;
        vfc::uint32_t m_FID_Temp_Blind             : 1;
        vfc::uint32_t m_FID_Permanent_Blind        : 1;
        vfc::uint32_t m_FID_HWFullOp               : 1;
        vfc::uint32_t m_FID_Cal_Extrinsic          : 1;
        vfc::uint32_t m_FID_Cal_Intrinsic          : 1;
        vfc::uint32_t m_FID_COM_CLF                : 1;
        vfc::uint32_t m_FID_COM_CV_Light           : 1;
        vfc::uint32_t m_FID_COM_RSR                : 1;
        vfc::uint32_t m_FID_COM_LANE               : 1;
        vfc::uint32_t m_FID_COM_EGMO               : 1;
        vfc::uint32_t m_FID_COM_ESCIMO             : 1;
        vfc::uint32_t m_FID_COM_AEB                : 1;
        vfc::uint32_t m_FID_COM_RSF_Vision         : 1;
        vfc::uint32_t m_FID_COM_ELKre              : 1;
        vfc::uint32_t m_FID_COM_RDP                : 1;
        vfc::uint32_t m_FID_COM_NAV_ADASTimeout    : 1;
        vfc::uint32_t m_FID_DSM_CALRuntime         : 1;
        vfc::uint32_t m_FID_DSM_CAL_FDD            : 1;
        vfc::uint32_t m_FID_DSM_CAL_Suspension     : 1;
        vfc::uint32_t m_FID_CV_LUKE                : 1;
        vfc::uint32_t m_FID_CV_PEPSI               : 1;
        vfc::uint32_t m_FID_CV_RSR                 : 1;
        vfc::uint32_t m_FID_CV_TOFU                : 1;
        vfc::uint32_t m_FID_DSM_COM_RADAR_Status   : 1;
        vfc::uint32_t m_FID_DC_OOS                 : 1;
        vfc::uint32_t m_FID_DC_QM                  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatCSrv_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatCDia_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatESrv_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatEDia_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LongES_Error   : 1;
    };
    St_Fids str ;
    vfc::uint32_t m_value_u32;
}UFIDStatus;



int main() {
    printf("Ok..\n");
    unsigned char temp = static_cast<unsigned char>(static_cast<unsigned char>(-10) + 40u);  
    // cout << temp;
    // printf("%d\n", temp);
    // printf("%x\n", temp);
    // printf("%d\n", sizeof(numm));
    // printf("%d\n", sizeof(98u));

    // to test the qacpp-4.7.0:3133
    printf("%llx\n", static_cast<int>((char)255 + (char)1));
    printf("%llx\n", static_cast<int>((char)1 << (char)10u));
    printf("%llx\n", ((char)1 << (char)31u)); // char 1 is automatically casted to int 
    printf("%llx\n", (static_cast<char>(1) << (char)31u)); // char 1 is automatically casted to int 
    printf("%llx\n", ((char)1 << (char)32u));
    printf("%llx\n", ((long long)1 << (char)32u));

    printf("to test for qacpp-4.7.0:3015\n");
    printf("%d\n", static_cast<unsigned char>(20300 / 0.08f / 1000u));
    printf("%d\n", static_cast<unsigned char>(20400 / 0.08f / 1000u));
    printf("%d\n", static_cast<unsigned char>(20900 / 0.08f / 1000u));

    printf("to test for qacpp-4.7.0:3016\n");
    printf("%d\n", static_cast<unsigned char>(300.0f));

    // array<int,3> a = {1,3,2};
    // array<int,3> b = {{1,3,2}};
    // printf("%x\n", &a);
    // printf("%x\n", &b);
    // array<array<int,3>, 2> aa = {a, a};
    // array<array<int,3>, 2> bb = {b, b};
    // array<array<int,3>, 2> cc = {{1,3,2}};
    // array<array<int,3>, 2> dd = {{1,3}};

    int a = 5;
    int* pa = &a;
    // pa = pa;

    printf("%x\n", static_cast<bool>(0x54u));
    printf("%x\n", static_cast<bool>(0x00u));
    printf("%x\n", static_cast<bool>(0x01u));


    printf("float\n");
    printf("-3276.8F: %x\n", -3276.8F);
    // printf("3276.7F: %x\n", 3276.7F);


    UFIDStatus test;
    test.str.m_FID_CV_TOFU = 0;
    test.str.m_FID_DSM_COM_RADAR_Status = 0;
    test.str.m_FID_DC_OOS = 1;
    test.str.m_FID_DC_QM = 0;
    printf("test.m_value_u32 = 0x%x\n", test.m_value_u32);

    return 0;
}