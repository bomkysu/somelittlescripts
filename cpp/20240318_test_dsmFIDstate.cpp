#include "stdio.h"
#include <iostream>

using namespace std;

namespace vfc
{
    typedef unsigned int uint32_t;
    typedef unsigned char uint8_t;
} // namespace name

typedef union
{
    struct St_Fids
    {
        vfc::uint32_t m_FID_Ubat                   : 1;
        vfc::uint32_t m_FID_Temp_Blind             : 1;
        vfc::uint32_t m_FID_Permanent_Blind        : 1;
        vfc::uint32_t m_FID_HWFullOp               : 1;
        vfc::uint32_t m_FID_Cal_Extrinsic          : 1;
        vfc::uint32_t m_FID_Cal_Intrinsic          : 1;
        vfc::uint32_t m_FID_COM_CLF                : 1;
        vfc::uint32_t m_FID_COM_CV_Light           : 1;
        vfc::uint32_t m_FID_COM_RSR                : 1;
        vfc::uint32_t m_FID_COM_LANE               : 1;
        vfc::uint32_t m_FID_COM_EGMO               : 1;
        vfc::uint32_t m_FID_COM_ESCIMO             : 1;
        vfc::uint32_t m_FID_COM_AEB                : 1;
        vfc::uint32_t m_FID_COM_RSF_Vision         : 1;
        vfc::uint32_t m_FID_COM_ELKre              : 1;
        vfc::uint32_t m_FID_COM_RDP                : 1;
        vfc::uint32_t m_FID_COM_NAV_ADASTimeout    : 1;
        vfc::uint32_t m_FID_DSM_CALRuntime         : 1;
        vfc::uint32_t m_FID_DSM_CAL_FDD            : 1;
        vfc::uint32_t m_FID_DSM_CAL_Suspension     : 1;
        vfc::uint32_t m_FID_CV_LUKE                : 1;
        vfc::uint32_t m_FID_CV_PEPSI               : 1;
        vfc::uint32_t m_FID_CV_RSR                 : 1;
        vfc::uint32_t m_FID_CV_TOFU                : 1;
        vfc::uint32_t m_FID_DSM_COM_RADAR_Status   : 1;
        vfc::uint32_t m_FID_DC_OOS                 : 1;
        vfc::uint32_t m_FID_DC_QM                  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatCSrv_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatCDia_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatESrv_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LatEDia_Error  : 1;
        vfc::uint32_t m_FID_DSM_VMC_LongES_Error   : 1;
    };
    St_Fids str ;
    vfc::uint32_t m_value_u32;
}UFIDStatus;

int main() {

    UFIDStatus test;
    test.str.m_FID_CV_TOFU = 0;
    test.str.m_FID_DSM_COM_RADAR_Status = 0;
    test.str.m_FID_DC_OOS = 1;
    test.str.m_FID_DC_QM = 0;
    printf("test.m_value_u32 = 0x%x\n", test.m_value_u32);

    struct testStr
    {
        vfc::uint8_t testStr0  : 1;
        vfc::uint8_t testStr1  : 1;
        vfc::uint8_t testStr2  : 1;
        vfc::uint8_t testStr3  : 5;    
    };
    testStr testStr32;
    testStr32.testStr0=7;
    testStr32.testStr1=0;
    testStr32.testStr2=2;
    testStr32.testStr3=3;

    printf("testStr32.testStr0 = 0x%x\n", testStr32.testStr0);
    printf("testStr32.testStr1 = 0x%x\n", testStr32.testStr1);
    printf("testStr32.testStr2 = 0x%x\n", testStr32.testStr2);
    printf("testStr32.testStr3 = 0x%x\n", testStr32.testStr3);

    printf("testStr::testStr0 size = %d\n", sizeof(testStr::testStr0));
    // printf("testStr32.testStr1 = 0x%x\n", sizeof(testStr32.testStr1));
    // printf("testStr32.testStr2 = 0x%x\n", sizeof(testStr32.testStr2));
    // printf("testStr32.testStr3 = 0x%x\n", sizeof(testStr32.testStr3));
    // vfc::uint32_t ui32 = static_cast<vfc::uint32_t>(testStr32);
    // printf("ui32 = 0x%x\n", ui32);

    return 0;
}