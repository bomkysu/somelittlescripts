#include "stdio.h"
#include <vector>
using namespace std;

void sort(unsigned char * arr, unsigned char size);

int main() {
    printf("Ok..\n");
    unsigned char arr[5] = {33,32,31,34,35};
    printf("%d\t", arr[0]);
    printf("%d\t", arr[1]);
    printf("%d\t", arr[2]);
    printf("%d\t", arr[3]);
    printf("%d\n", arr[4]);
    sort(arr, 5);
    printf("%d\t", arr[0]);
    printf("%d\t", arr[1]);
    printf("%d\t", arr[2]);
    printf("%d\t", arr[3]);
    printf("%d\n", arr[4]);

    vector<float> v;
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());
    v.push_back(1);
    printf("capacity: %d\tsize: %d\n", v.capacity(), v.size());


    return 0;
}

void sort(unsigned char * arr, unsigned char size)
{
    for (int i = 0; i < size; ++i)
    {
        for (int j = i+1; j < size; ++j)
        {
            if (arr[i] > arr[j])
            {
                unsigned char temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}

// Time complexity: O(n.log(n)) // WHY???? 
// Space complexity: O(1) // OK 

// g++ 20231123_example_sort.cpp -o _run -std=c++17 && ./_run.exe

