#include <iostream>
using namespace std;
 
 


struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int _val): val(_val){}
};

void printList(ListNode *head)
{
    while (head!= nullptr)
    {
        cout << "addr: " << head << "\t";
        cout << "val: " << head->val << endl;
        head = head->next;
    }
}

//return 0 if success, -1 if fail
int swap2Node (ListNode **head, int pos1, int pos2)
{
    ListNode *curr = nullptr;
    int index = 0;
    ListNode *nodePos1Prev = nullptr;
    ListNode *nodePos1 = nullptr;
    ListNode *nodePos2 = nullptr;
    ListNode *nodePos2Next = nullptr;

    // add headTemp to swap index 0
    ListNode *headTemp = new ListNode(85);
    headTemp->next = *head;
    curr = headTemp;
    --index; 

    while (nullptr != curr)
    {
        if (index == pos1 - 1)
        {
            // backup nodePos1
            nodePos1Prev = curr;
            cout << "nodePos1Prev" << endl;
            printList(nodePos1Prev);
            nodePos1 = curr->next;
            curr->next = curr->next->next; // remove nodePos1 from list
            nodePos1->next = nullptr; // remove nodePos1 from list
            cout << "nodePos1" << endl;
            printList(nodePos1);
        }

        if (index == pos2 - 2)  // -2 because nodePos1 was removed from list
        {
            // backup nodePos2
            nodePos2 = curr->next;
            nodePos2Next = curr->next->next;
            nodePos2->next = nullptr; // remove nodePos1 from list

            // add nodePos1 to list
            curr->next = nodePos1; 
            nodePos1->next = nodePos2Next;

            cout << "nodePos2" << endl;
            printList(nodePos2);
            cout << "nodePos2Next" << endl;
            printList(nodePos2Next);

            // add nodePos2 to list
            nodePos2->next = nodePos1Prev->next;
            cout << "nodePos2" << endl;
            printList(nodePos2);
            nodePos1Prev->next = nodePos2;
        }
        index ++;
        curr = curr->next;
    }
    cout << "headTemp" << endl;
    printList(headTemp);
    *head = headTemp->next;
    cout << "head" << endl;
    printList(*head);
    return 0;
}

int main()
{
    ListNode *node1 = new ListNode(0);
    ListNode *node2 = new ListNode(1);
    ListNode *node3 = new ListNode(2);
    ListNode *node4 = new ListNode(3);
    ListNode *node5 = new ListNode(4);

    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = node5;
    node5->next = nullptr;
    // node5->next = new ListNode(5);
    // node5->next->next = nullptr;

    cout << "node1 before" << endl;
    printList(node1);
    cout << endl;
    cout << swap2Node(&node1, 0, 2) << endl;
    cout << endl;
    cout << "node1 after" << endl;
    printList(node1);

    return 0;
 
}