
Data Structure + Algorithm + OOP

    Data Structure
        Array
        Linked list
        stack
        queue
        dequeue
        tree
        hash map (unordered map/set)
        map/set
        graph
        heap - priority

    Algorithm
        Binary search
        Sort
        Swap
        reverse
        backtracking
        dynamic programming
        find
        replace


Big O notation - complexity
    Time complexity
    Space complexity

    Example: 
    int arr[n]
    sort(arr)
    {
        for (....;idx < n; ++idx)
        {
            // .... 
            // no break
        }
    }

    Time: O(n)  // n --> infinity
        Because this block loop n times
        BUT, 
        if n = 1 or 2, 3, 10, .... Time complexity also O(n), never O(1), O(2), .... 
    Space: O(1)

    If 2 cascaded for loop: O(n^2)
    If 2 coutinuous for loop: O(n)

/////////////////////////////////////////////////
Memory field: 
    Compiling time: BSS / Data / RAM 
    Runtime: Stack / Heap

/////////////////////////////////////////////////
Array                                       Vector
                       Common
                  - Random access
                  - Contiguous memory location
- Static allocation                         - Dinamic allocation



int arr[10];                                vector<int> vec[10];

0..3 : arr[0]                               0..3 : vec[0]
4..7 : arr[1]                               4..7 : vec[1]

                                            vector<int> vec[2];
                                            vec.push_back(1);    // size: 2 // capacity: 4 
                                            >> This is called re-allocate 



/////////////////////////////////////////////////
Compile and run: 
g++ 20231123_example_sort.cpp -o _run -std=c++17 && ./_run.exe
