#include <iostream>
using namespace std;
 
 


struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int _val): val(_val){}
};

void printList(ListNode *head)
{
    while (head!= nullptr)
    {
        cout << "addr: " << head << "\t";
        cout << "val: " << head->val << endl;
        head = head->next;
    }
}

//return 0 if success, -1 if fail
int swap2Node (ListNode *head, int pos1, int pos2)
{
    ListNode *curr;
    int index = 0;
    curr = head;
    ListNode *nodePos1Prev = nullptr;
    ListNode *nodePos1 = nullptr;
    ListNode *nodePos2 = nullptr;
    ListNode *nodePos2Next = nullptr;

    while (nullptr != curr)
    {
        if (index == pos1 - 2)
        {
            nodePos1Prev = curr->next;
            // nodePos1Prev->next = nullptr;
            cout << "nodePos1Prev" << endl;
            printList(nodePos1Prev);
        }
        if (index == pos1 - 1)
        {
            // cout << "index " << index << endl;
            // cout << curr->val << endl;
            
            nodePos1 = curr->next;
            curr->next = curr->next->next; // remove nodePos1 from list
            nodePos1->next = nullptr; // remove nodePos1 from list
            cout << "nodePos1" << endl;
            printList(nodePos1);
        }
        
        if (index == pos2 - 2)  // -2 because nodePos1 was removed from list
        {
            nodePos2 = curr->next;
            nodePos2Next = curr->next->next;
            curr->next = nodePos1; // add nodePos1 to list
            nodePos1 = nodePos2Next;
            nodePos2->next = nullptr; // remove nodePos2 from list
            cout << "nodePos2" << endl;
            printList(nodePos2);

        }
        // if (index == pos2 - 1)
        // {
        //     cout << "index " << index << endl;
        //     cout << "curr addr " << curr << endl;
        //     cout << "curr val " << curr->val << endl;
        //     cout << "next addr " << curr->next << endl;
        //     // cout << "next val " << curr->next->val << endl;
        //     // cout << "next next addr " << curr->next->next << endl;
        //     nodePos2Next = curr->next;
        //     curr->next = nodePos1;
        //     nodePos1->next = nodePos2Next;
        // }
        index ++;
        curr = curr->next;

    }
    // curr->next = nodePos1;
    return 0;
}

int main()
{
    ListNode *node1 = new ListNode(0);
    ListNode *node2 = new ListNode(1);
    ListNode *node3 = new ListNode(2);
    ListNode *node4 = new ListNode(3);
    ListNode *node5 = new ListNode(4);
 
 
    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = node5;
    node5->next = new ListNode(5);
    node5->next->next = nullptr;
    // printList(node1);
    swap2Node(node1, 2, 4);
    cout << endl;
    printList(node1);
    // cout << endl;
    // printList(node1->next->next->next);

    return 0;
 
}