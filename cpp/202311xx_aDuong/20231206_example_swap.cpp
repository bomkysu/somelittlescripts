#include <iostream>
using namespace std;
 
 


struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int _val): val(_val){}
};

void printList(ListNode *head)
{
    while (head!= nullptr)
    {
        cout << "addr: " << head << "\t";
        cout << "val: " << head->val << endl;
        head = head->next;
    }
}

//return 0 if success, -1 if fail
int swap2Node (ListNode **head, int pos1, int pos2)
{
    if ((nullptr == *head) || (0 > pos1) || (pos1 >= pos2))
    {
        return -1;
    }
    ListNode *curr = nullptr;
    int index = 0;
    ListNode *nodePos1Prev = nullptr;
    ListNode *nodePos1 = nullptr;
    ListNode *nodePos2 = nullptr;
    ListNode *nodePos2Next = nullptr;

    // prepend buffer to swap index 0
    // ListNode *headTemp = new ListNode(85); // HEAP // CAN CAUSE MEMORY LEAK 
    ListNode vheadTemp(85); // STACK // CREATE AT RUNTIME
    ListNode *headTemp = &vheadTemp; //headTemp->val = 85; // STACK // CREATE AT RUNTIME
    headTemp->next = *head;
    curr = headTemp;
    --index; 

    while (nullptr != curr->next)
    {
        if (index == pos1 - 1)
        {
            // backup nodePos1Prev nodePos1 
            nodePos1Prev = curr;
            nodePos1 = curr->next;

            // remove nodePos1 from list
            curr->next = curr->next->next;
            nodePos1->next = nullptr;
        }

        if (index == pos2 - 2)  // -2 because nodePos1 was removed from list
        {
            // backup nodePos2
            nodePos2 = curr->next;
            
            // backup nodePos2Next
            nodePos2Next = curr->next->next;

            // remove nodePos2 from list
            nodePos2->next = nullptr;

            // add nodePos1 to list at old pos2 position 
            curr->next = nodePos1;
            nodePos1->next = nodePos2Next;

            // add nodePos2 to list at old pos1 position 
            nodePos2->next = nodePos1Prev->next;
            nodePos1Prev->next = nodePos2;
        }
        index ++;
        curr = curr->next;
    }

    // remove prepended buffer
    *head = headTemp->next; 
    return 0;
}

int main()
{
    ListNode *node0 = new ListNode(0);
    ListNode *node1 = new ListNode(1);
    ListNode *node2 = new ListNode(2);
    ListNode *node3 = new ListNode(3);
    ListNode *node4 = new ListNode(4);

    node0->next = node1;
    // node1->next = nullptr;
    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = nullptr;
    // node4->next = new ListNode(5);
    // node4->next->next = nullptr;

    cout << "node0 before" << endl;
    printList(node0);
    cout << "swap result: " << swap2Node(&node0, 0, 1) << endl;
    cout << "node1 after" << endl;
    printList(node0);

    cin.get();
    return 0;
}

/*
node1 before
addr: 0x1002780 val: 0
addr: 0x10027a0 val: 1
addr: 0x1002af0 val: 2
addr: 0x1002b10 val: 3
addr: 0x1002b30 val: 4
swap result: 0
node1 after
addr: 0x1002780 val: 0
addr: 0x10027a0 val: 1
addr: 0x1002b30 val: 4
addr: 0x1002b10 val: 3
addr: 0x1002af0 val: 2
*/

