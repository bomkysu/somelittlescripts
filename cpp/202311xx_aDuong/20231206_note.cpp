

lvalue
    can be assigned a value 

rvalue
    can not be assigned a value 
    only return as a move-constructor 


lvalue = rvalue

int a = 5;
int &b = a; // get a as lvalue 
int &&c = a; // get a as rvalue



/// OPL:
write a move constructor, move assignment operator 
investigate std::move / std::forward





//////////////////////////////////
LINK LIST 

#include <list> // doubly // there are *next and *previous 
#include <forward_list> // singly 

sizeof(int) == data bus 
sizeof(int*) == address bus




