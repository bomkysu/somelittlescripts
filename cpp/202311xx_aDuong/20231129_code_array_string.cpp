#include <vector>
#include <iostream>
#include <string>
// #include "string.h" // c

using namespace std;

bool checkSumEqual(vector<int> v, unsigned int sum);

int main() 
{
    vector<int> v = {1, 2, 3, 4};
    // vector<int> v = {1, 3, 4};
    int sum = 6;
    cout << "OK" << endl;
    cout << checkSumEqual(v, sum) << endl;
    return 0;
}

bool checkSumEqual(vector<int> v, unsigned int sum)
{
    int left = 0;
    int right = v.size() - 1;
    while (left < right)
    {
        if (v[left] + v[right] == sum)
        {
            return true;
        }
        else if (v[left] + v[right] < sum)
        {
            left++; // to increase the calculated sum
        }
        else
        {
            right--; // to decrease the calculated sum
        }
    }
    return false;
}

