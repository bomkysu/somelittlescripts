// Prefix Sum 

// Exercise 1: Given an array of integers nums, you start with an initial positive value startValue.
// In each iteration, 
// you calculate the step by step sum of startValue plus elements in nums (from left to right).
// Return the minimum positive value of startValue such that the step by step sum is never less than 1.

#include <vector>
#include <iostream>
#include <string>

using namespace std;

double findMaxAverage(vector<int>& nums, int k);

int main() 
{
    vector<int> v = {1, 2, 3, 4, 2, 9, 1, 10, 3, 2, 1};
    int windowSize = 200;
    // cout << "OK" << endl;
    cout << findMaxAverage(v, windowSize) << endl;
    return 0;
}

double findMaxAverage(vector<int>& nums, int k)
{
    int left = 0, right = 0;
    double ans = 0, average = 0;

    for (left = 0, right = 0; right < nums.size(); right++)
    {
        average += (double)nums[right]/k;   // sum of weighting factor instead of average calculating
        if (right > k-1)
        {
            average -= (double)nums[left]/k; // move the window forward 
            left++;
        }
        ans = max(ans, average);
        // cout << average << endl;
    }

    return ans;
}

