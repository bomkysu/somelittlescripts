// Sliding Windows

// Exercise 1: You are given an integer array nums consisting of n elements, and an integer k.
//      Find a contiguous subarray whose length is equal to k, 
//      that has the maximum average value and return this value. 
//      Function: double findMaxAverage(vector<int>& nums, int k)

// g++ 20231206_arry_string_excercise_sliding_windows.cpp  -o _run -std=c++17 && ./_run.exe

#include <vector>
#include <iostream>
#include <string>

using namespace std;

double findMaxAverage(vector<int>& nums, int k);

int main() 
{
    vector<int> v = {1, 2, 3, 4, 2, 9, 1, 10, 3, 2, 1};
    int windowSize = 3;
    // cout << "OK" << endl;
    cout << findMaxAverage(v, windowSize) << endl;
    return 0;
}

double findMaxAverage(vector<int>& nums, int k)
{
    int left = 0, right = 0;
    double ans = 0, average = 0;
    k = min(k, (int)nums.size());
    for (left = 0, right = 0; right < nums.size(); right++)
    {
        average += (double)nums[right]/k;   // sum of weighting factor instead of average calculating
        if (right > k-1)
        {
            average -= (double)nums[left]/k; // move the window forward 
            left++;
        }
        ans = max(ans, average);
        // cout << average << endl;
    }

    return ans;
}

