


20231129_code_array_string.cpp

g++ 20231129_code_array_string.cpp -o _run -std=c++17 && ./_run.exe



[4:36 PM] Nguyen Trung Duong (MS/EDA2-XC)

Given a sorted array of unique integers and a target integer, return true if here exists a pair of numbers that sum to target, false otherwise.

For example, given nums = [1, 2, 4, 6 , 8, 9, 14, 15], target = 13. => true (4+9)



