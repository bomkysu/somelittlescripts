


    ListNode *headTemp = new ListNode(85); // HEAP // CAN CAUSE MEMORY LEAK 
vs 
    ListNode vheadTemp(85); // STACK // CREATE AT RUNTIME
    ListNode *headTemp = &vheadTemp; //headTemp->val = 85; // STACK // CREATE AT RUNTIME
stack/heap


smart pointer 

unique_ptr
    can not be moved 
shared_ptr
    used in multi-thread



//////////////
LAMBDA EXPRESSION
inline function

//////////////
CAST

implicit type conversion
    (int)a

explicit type conversion
    static_cast
    dynamic_cast
    reinterpret_cast
    const_cast

static_cast
    if a class X inherit from 2 class A, class B 
    static_cast from X to A 
    will not have the same address 
    static_cast from X to B 

const_cast
    a const X after use const_cast, can by pass compiler error when using X = 1 / X = 2 ,.... 
    BUT, value never be changed 

    be used to prevent code duplication for member functions 

dynamic_cast
    be used to check the correct type of a object 
    if object type and <type> are different, return nullptr 
    require RTTI 

reinterpret_cast
    try to cast any type to any type

implicit type conversion
    this will try to use the most compatible cast: 
        const_cast
            static_cast
                dynamic_cast
                    reinterpret_cast
                        const_cast reinterpret_cast

///////////////////////
STACK vs QUEUE

STACK
    this just a term
    can be deployed by: list (DOUBLE LINK LIST), vector, deque .... 
    LIFO
    std::stack
    stack.push()
    stack.pop()

QUEUE
    same STACK
    but FIFO
    can not deployed by vector
    enqueue: add data
    dequeue: remove data

Monotonic
    Sorted STACK or QUEUE
