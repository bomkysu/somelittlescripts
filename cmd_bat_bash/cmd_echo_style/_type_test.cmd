
type Reset.txt
type Bold.txt
type Underline.txt
@REM type Inverse.txt

@REM echo|set /p dummy=[101;93m NORMAL FOREGROUND COLORS>
type text_Black.txt
type text_Red.txt
type text_Green.txt
type text_Yellow.txt
type text_Blue.txt
type text_Magenta.txt
type text_Cyan.txt
type text_White.txt

@REM echo|set /p dummy=[101;93m NORMAL BACKGROUND COLORS>
type background_Black.txt
type background_Red.txt
type background_Green.txt
type background_Yellow.txt
type background_Blue.txt
type background_Magenta.txt
type background_Cyan.txt
type background_White.txt

@REM echo|set /p dummy=[101;93m STRONG FOREGROUND COLORS>
type text_brighter_Black.txt
type text_brighter_Red.txt
type text_darker_Green.txt
type text_brighter_Yellow.txt
type text_brighter_Blue.txt
type text_brighter_Magenta.txt
type text_brighter_Cyan.txt
type text_brighter_White.txt

@REM echo|set /p dummy=[101;93m STRONG BACKGROUND COLORS>
type background_brighter_Black.txt
type background_brighter_Red.txt
type background_darker_Green.txt
type background_brighter_Yellow.txt
type background_brighter_Blue.txt
type background_brighter_Magenta.txt
type background_brighter_Cyan.txt
type background_brighter_White.txt

@REM echo|set /p dummy=[101;93m COMBINATIONS>
type red_foreground_color.txt
@REM type inverse_foreground_background.txt
@REM type inverse_red_foreground_color.txt
type 7mbefore_31mnested.txt
type 31mbefore_7mnested.txt