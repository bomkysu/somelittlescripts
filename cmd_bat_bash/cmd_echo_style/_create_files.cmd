
echo|set /p dummy=[0m>Reset.txt
echo|set /p dummy=[1m>Bold.txt
echo|set /p dummy=[4m>Underline.txt
echo|set /p dummy=[7m>Inverse.txt

@REM echo|set /p dummy=[101;93m NORMAL FOREGROUND COLORS>
echo|set /p dummy=[30m>text_Black.txt
echo|set /p dummy=[31m>text_Red.txt
echo|set /p dummy=[32m>text_Green.txt
echo|set /p dummy=[33m>text_Yellow.txt
echo|set /p dummy=[34m>text_Blue.txt
echo|set /p dummy=[35m>text_Magenta.txt
echo|set /p dummy=[36m>text_Cyan.txt
echo|set /p dummy=[37m>text_White.txt

@REM echo|set /p dummy=[101;93m NORMAL BACKGROUND COLORS>
echo|set /p dummy=[40m>background_Black.txt
echo|set /p dummy=[41m>background_Red.txt
echo|set /p dummy=[42m>background_Green.txt
echo|set /p dummy=[43m>background_Yellow.txt
echo|set /p dummy=[44m>background_Blue.txt
echo|set /p dummy=[45m>background_Magenta.txt
echo|set /p dummy=[46m>background_Cyan.txt
echo|set /p dummy=[47m>background_White.txt

@REM echo|set /p dummy=[101;93m STRONG FOREGROUND COLORS>
echo|set /p dummy=[90m>text_brighter_Black.txt
echo|set /p dummy=[91m>text_brighter_Red.txt
echo|set /p dummy=[92m>text_darker_Green.txt
echo|set /p dummy=[93m>text_brighter_Yellow.txt
echo|set /p dummy=[94m>text_brighter_Blue.txt
echo|set /p dummy=[95m>text_brighter_Magenta.txt
echo|set /p dummy=[96m>text_brighter_Cyan.txt
echo|set /p dummy=[97m>text_brighter_White.txt

@REM echo|set /p dummy=[101;93m STRONG BACKGROUND COLORS>
echo|set /p dummy=[100m>background_brighter_Black.txt
echo|set /p dummy=[101m>background_brighter_Red.txt
echo|set /p dummy=[102m>background_darker_Green.txt
echo|set /p dummy=[103m>background_brighter_Yellow.txt
echo|set /p dummy=[104m>background_brighter_Blue.txt
echo|set /p dummy=[105m>background_brighter_Magenta.txt
echo|set /p dummy=[106m>background_brighter_Cyan.txt
echo|set /p dummy=[107m>background_brighter_White.txt

@REM echo|set /p dummy=[101;93m COMBINATIONS>
echo|set /p dummy=[31m>red_foreground_color.txt
echo|set /p dummy=[7m>inverse_foreground_background.txt
echo|set /p dummy=[7;31m>inverse_red_foreground_color.txt
echo|set /p dummy=[7m [31m>7mbefore_31mnested.txt
echo|set /p dummy=[31m [7m>31mbefore_7mnested.txt