
@REM #### THIS SCRIPT BE USED FOR MPC3 PROJECTS 
@REM #### Author: Nguyen Huu Nghia (MS/EDA21-XC) 
@REM #### Purpose: Help extract binaries FUCKING FASTER and FUCKING STORAGE-SAVING..  



"C:\Program Files\7-Zip\7z.exe"  x cac281d_mc1_dev.7z -ocac281d_mc1_dev_filtered *bin\*.bin *bin\*.xbin *bin\*.xml -r
"C:\Program Files\7-Zip\7z.exe"  e cac281d_mc1_dev.7z -ocac281d_mc1_dev_filtered\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r
// "C:\Program Files\7-Zip\7z.exe"  e cac281d_mc1_dev.7z -ocac281d_mc1_dev_filtered\bin *bin\* -r

"C:\Program Files\7-Zip\7z.exe"  e \\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-46140\3\cac281d_mc1_dev.7z -o\\hc-c-00183.hc.apac.bosch.com\nun4hc\Sandboxes\CA\C281D\test *bin\*.bin *bin\*.xbin *bin\*.xml -r


"C:\Program Files\7-Zip\7z.exe"  e cac281d_mc0_release.7z -ocac281d_mc0_release\bin  *bin\*.bin *bin\*.xbin *bin\*.xml -r

"C:\Program Files\7-Zip\7z.exe"  e \\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_C281D_BL02_RC04\cac281d_mc0_dev.7z -omc0_dev_f *bin\*.bin *bin\*.xbin *bin\*.xml -r


//// THISSSSSSSSS.....
"C:\Program Files\7-Zip\7z.exe"  e cac281d_mc0_dev.7z -ocac281d_mc0_dev_filtered\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r
"C:\Program Files\7-Zip\7z.exe"  e cab561_mc0_release.7z -ocab561_mc0_release_filtered\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r
"C:\Program Files\7-Zip\7z.exe"  e *.7z -ofiltered_bin\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r
"C:\Program Files\7-Zip\7z.exe"  e cacd569ica_mc0_dev.7z -ocacd569ica_mc0_dev_filtered\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r
"C:\Program Files\7-Zip\7z.exe"  e \\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-46961\1\cacd569ica_mc0_dev.7z -oPR_46961_1_mc0_dev_filtered\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r

xcopy "\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-49613\1\cas311_mc1_dev.7z" temp_xcopy
robocopy "\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-49613\1" "temp_robocopy" "cas311_mc1_dev.7z"

"C:\Program Files\7-Zip\7z.exe"  e "\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-49613\1\cas311_mc1_dev.7z" -ofiltered\bin *bin\*.bin *bin\*.xbin *bin\*.xml -r

////// COMPRESS....
"C:\Program Files\7-Zip\7z.exe" a 20220727_1147.zip -ir!*bin\*.bin -ir!*bin\*.xbin -ir!*bin\*.xml
"C:\Program Files\7-Zip\7z.exe" a filtered_bin.zip -ir!*bin\*.bin -ir!*bin\*.xbin -ir!*bin\*.xml
"C:\Program Files\7-Zip\7z.exe" a 20220727_1147.7z -ir!*bin\*.bin -ir!*bin\*.xbin -ir!*bin\*.xml
"C:\Program Files\7-Zip\7z.exe" a 20220805_1646.zip -ir!*bin\*.bin -ir!*bin\*.xbin -ir!*bin\*.xml -ir!*bin*\*.xls
"C:\Program Files\7-Zip\7z.exe" a 20220805_1651.zip -ir!*bin\*.bin -ir!*bin\prmArtifacts\GenDataset\* 
"C:\Program Files\7-Zip\7z.exe" a xcvxcvxcvxcvxv.zip -ir!*bin\*.bin -ir!*bin\prmArtifacts\GenDataset\* 

// .zip is faster than .7z.. 


REM...... https://inside-docupedia.bosch.com/confluence/pages/viewpage.action?pageId=2431363348

@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_S311_BL01_RC05\cas311_mc1_series.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_S311_BL01_RC05\SmartCard_Binaries\cas311_mc1_series_KMS_ON.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_C385_BL03_RC03\cac385_mc1_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_C385_BL03_RC10\cac385_mc1_release.7z"
@REM set sourcePath="\\ABTVDFS2.DE.BOSCH.COM\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_B561_FV0230_BL03V2\cab561_mc1_series.7z"
@REM set sourcePath="\\ABTVDFS2.DE.BOSCH.COM\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_B561_FV0230_BL03V1\cab561_mc1_series.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_PF\all\FV0230_v15.0\pfmono_mc1pf_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_C281D_BL04_RC05\cac281d_mc1_series.7z"
@REM set sourcePath-="\\bosch.com\dfsRB\DfsVN\LOC\Hc\RBVH\20_EDA\10_EDA1\01_Internal\11_Projects\Video_Releases\CA_C385_BL03_RC12\Smartcard_Binaries\cac385_mc1_series_KMS_ON.7z"
@REM set sourcePath="\\bosch.com\dfsRB\DfsVN\LOC\Hc\RBVH\20_EDA\10_EDA1\01_Internal\11_Projects\Video_Releases\CA_C673_BL01_RC06\Smartcard_Binaries\cac673_mc1_series_KMS_ON.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_C673_BL01_RC06\cac673_mc1_series.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-59093\1\cac673_mc1_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-58842\2\cac385_mc1_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-62913\7\mievmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\5H_V3.1.6\bsuvmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\2N_V4.0.5\mievmono_mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\5H_V3.1.7\bsuvmono_mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-64193\1\bsuvmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-65537\1\mievmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-65537\1\bsuvmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-65537\1\m_5jmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-65537\1\m_5vmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-66178\1\bsuvmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-66270\10\bsuvmono_mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\5J_V5.0.3\m_5jmono_mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\5H_V3.1.8\bsuvmono_mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_HONDA\all\HJ3202V2\hmono_mc1h_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\5V_V7.0.1\m_5vmono_mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_HONDA\all\HJ3203V2\hmono_mc1h_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\5V_V7.0.2\m_5vmono_mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Dev\01_all\jenkins\builds\jmaas\G3N_2\multibranch\fvg3_lfs_target\PR-71697\1\c673_mc1_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\4Y_V1.0.1\m_4ymono_cp1mc1m_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\4Y_V1.0.1\m_4ymono_cp1mc1m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_MMC\all\4Y_V1.0.1\m_4ymono_cp1mc0m_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_HONDA\all\H2202V2.10\hmono_mc1h_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_HONDA\all\HJ3303V1\hmono_mc1h_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Cus_Honda_MY22\01_all\09_app\11_SW_MPC3\H2204Tx\H2204T2.9_LKAS_Issue_2\hmono_mc1h_dev.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_HONDA\all\H2204V2.6\hmono_mc1h_release.7z"
@REM set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\ida\abt\video\Video_Rel\FVG3_REL_CUS_HONDA\all\H2110V4.13\hmono_mc1h_release.7z"
set sourcePath="\\abtvdfs2.de.bosch.com\ismdfs\loc\szh\DA\Driving\SW_TOOL_Release\G3N_Releases\CA_S311_BL01_RC05\cas311_mc1_release.7z"
 
for %t in (%sourcePath%) do set filteredFolder="%~nt_filtered"

@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\5H_V3.1.6"
@REM set destinationPath="\\HC-C-00183.HC.APAC.BOSCH.COM\nun4hc\PROJECTS\_MPC3\J_MMC\PR-63810_1"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\5H_V3.1.7"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\PR-65537_1"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\PR-66178-1"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\5J_V5.0.3"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\5H_V3.1.8"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_Honda\HJ3302V3"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_Honda\HJ3202V2"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\5V_V7.0.1"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_Honda\HJ3203V2"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\5V_V7.0.2"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\CA\PR-71697"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_MMC\4Y_V1.0.1"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_Honda\H2202V2.10"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_Honda\HJ3303V1"
@REM set destinationPath="\\HC-C-00183.HC.APAC.BOSCH.COM\nun4hc\PROJECTS\_MPC3\J_Honda\H2204T2.9_LKAS_Issue_2"
@REM set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\J_Honda\H2204V2.6"
set destinationPath="\\hc-c-00633.hc.apac.bosch.com\GEN3\nun4hc\PROJECTS\_MPC3\CA\CA_S311_BL01_RC05"

 
"C:\Program Files\7-Zip\7z.exe" e %sourcePath% -o%filteredFolder%\bin *bin\*.bin *bin\*.xbin *bin\*.xml *bin\int_rpu-arm-ac6* -r
"C:\Program Files\7-Zip\7z.exe" e %sourcePath% -o%filteredFolder%\pdm_dataitems *pdm_dataitems\* -r
"C:\Program Files\7-Zip\7z.exe" e %sourcePath% -o%filteredFolder%\prmArtifacts *prm*\*\*\*.xml *prm*\*\*\*\*.xls -r
 
"C:\Program Files\7-Zip\7z.exe" a %filteredFolder%.zip -ir!%filteredFolder%\bin\*.bin -ir!%filteredFolder%\bin\*.xbin -ir!%filteredFolder%\bin\*.xml 
"C:\Program Files\7-Zip\7z.exe" a %filteredFolder%.zip -ir!%filteredFolder%\pdm_dataitems\* 
"C:\Program Files\7-Zip\7z.exe" a %filteredFolder%.zip -ir!%filteredFolder%\prmArtifacts\* 
 
robocopy . %destinationPath% "%filteredFolder%.zip"

REM show all files path in folder: 
@REM echo off &  (for /r %i in (*) do echo %~fi) & echo on


