@echo off
setLocal EnableDelayedExpansion

set /p filename="Please enter the file path: "

rem Store text in array
set i=0
for /F "tokens=*" %%a in (%filename%) do (
  set /A i+=1
  set "text[!i!]=%%a"
)

rem Sort array
for /L %%i in (1,1,%i%) do (
  for /L %%j in (1,1,%i%) do (
    if "!text[%%i]!" gtr "!text[%%j]!" set "temp=!text[%%i]!"&set "text[%%i]=!text[%%j]!"&set "text[%%j]=!temp!"
  )
)

rem Output array
for /L %%i in (1,1,%i%) do echo !text[%%i]!

pause 