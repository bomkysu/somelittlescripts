#### THIS SCRIPT BE USED FOR MPC3 CA (ChangAn) PROJECTS 
#### Author: Nguyen Huu Nghia (MS/EDA21-XC) 
#### Purpose: Update DID F189 faster 

import re
import shutil

## <OptionalInput>
# # B561
# oldF189 = 'SWB.5.0'
# newF189 = 'SWB.5.1'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA"
# variant = "cab561"
# # B561 2.0T
# oldF189 = 'SWC.2.5'
# newF189 = 'SWC.2.6'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_B561_230"
# variant = "cab561"
# # CD569ICA 
# oldF189 = 'SWB.6.7'
# newF189 = 'SWB.6.8'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CD569ICA"
# variant = "cacd569ica"
# # S202MCA 
# oldF189 = 'SWC.4.5'
# newF189 = 'SWC.4.6'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CD569ICA"
# variant = "cas202mca"
# C385 
oldF189 = 'SWD.1.0'
newF189 = 'SWD.1.1'
rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CD569ICA"
variant = "cac385"
# # C281D
# oldF189 = 'SWC.7.1'
# newF189 = 'SWC.7.2'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_C281D"
# variant = "cac281d"
# # # C385
# oldF189 = 'SWC.9.0'
# newF189 = 'SWB.1.0'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_C281D"
# variant = "cac385"
# # C281PHEV
# oldF189 = 'SWC.6.1'
# newF189 = 'SWD.9.9'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_C281PHEV"
# variant = "cac281phev"
# S311
# oldF189 = 'SWC.7.1'
# newF189 = 'SWC.7.2'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_S311"
# variant = "cas311"
## </OptionalInput>

## <ov_dia_UserBswServiceTypes.hpp>
UBSTHpp_filePath = rootPath + "\cpj_ca\ov_pdm\inc\gen\\" + variant + "\ov_dia_UserBswServiceTypes.hpp"
## </ov_dia_UserBswServiceTypes.hpp>

## <ov_pdm_dataitem_config.cpp>
DICFGCpp_filePath = rootPath + "\cpj_ca\ov_pdm\inc\gen\\" + variant + "\ov_pdm_dataitem_config.cpp"
## </ov_pdm_dataitem_config.cpp>

## <ov_pdm_dataitem_handling.cpp>
DIHDLCpp_filePath = rootPath + "\cpj_ca\ov_pdm\inc\gen\\" + variant + "\ov_pdm_dataitem_handling.cpp"
## </ov_pdm_dataitem_handling.cpp>

## <OverWriteGenFiles>
try:
    shutil.copy(UBSTHpp_filePath, rootPath + "\cpj_ca\ov_pdm\gen\ov_dia_UserBswServiceTypes.hpp")
    shutil.copy(DICFGCpp_filePath, rootPath + "\cpj_ca\ov_pdm\gen\ov_pdm_dataitem_config.cpp")
    shutil.copy(DIHDLCpp_filePath, rootPath + "\cpj_ca\ov_pdm\gen\ov_pdm_dataitem_handling.cpp")
    shutil.copy(DICFGCpp_filePath, rootPath + "\cpj_ca\\tools\InsertTool\PDM\PdmGen\cfg\ov_pdm_dataitem_config.cpp")
    print("File copied successfully.")
except shutil.SameFileError:
    print("Source and destination represents the same file.")
except PermissionError:
    print("Permission denied.")
except:
    print("Error occurred while copying file.")
## </OverWriteGenFiles>

raw_input("ENTER to exit...")
