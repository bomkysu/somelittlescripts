#### THIS SCRIPT BE USED FOR MPC3 CA (ChangAn) PROJECTS 
#### Author: Nguyen Huu Nghia (MS/EDA21-XC) 
#### Purpose: Update DID F189 faster 

import re
import shutil

## <OptionalInput>
# # B561
# oldF189 = 'SWB.5.0'
# newF189 = 'SWB.5.1'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA"
# variant = "cab561"
# # B561 2.0T
# oldF189 = 'SWC.2.6'
# newF189 = 'SWC.2.7'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_B561_230"
# variant = "cab561"
# # CD569ICA 
# oldF189 = 'SWB.6.7'
# newF189 = 'SWB.6.8'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CD569ICA"
# variant = "cacd569ica"
# # S202MCA 
# oldF189 = 'SWC.4.5'
# newF189 = 'SWC.4.6'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CD569ICA"
# variant = "cas202mca"
# C385 
oldF189 = 'SWD.1.0'
newF189 = 'SWD.2.0'
rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CD569ICA"
variant = "cac385"
# # C281D
# oldF189 = 'SWC.7.1'
# newF189 = 'SWC.7.2'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_C281D"
# variant = "cac281d"
# # # C385
# oldF189 = 'SWC.9.0'
# newF189 = 'SWB.1.0'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_C281D"
# variant = "cac385"
# # C281PHEV
# oldF189 = 'SWC.6.1'
# newF189 = 'SWD.9.9'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_C281PHEV"
# variant = "cac281phev"
# S311
# oldF189 = 'SWC.7.1'
# newF189 = 'SWC.7.2'
# rootPath = "D:\PROJECTS\MPC3\CA\\fvg3_lfs_CA_S311"
# variant = "cas311"
## </OptionalInput>

oldF189_list = list(oldF189)
print(oldF189_list)
oldF189_list_hex = (hex(ord(char)) for char in oldF189_list)
oldF189_list_hex = list(oldF189_list_hex)
print(oldF189_list_hex)
oldF189_list_hex_join = "U,".join(oldF189_list_hex) + "U,0x00U"
print(oldF189_list_hex_join)

newF189_list = list(newF189)
print(newF189_list)
newF189_list_hex = (hex(ord(char)) for char in newF189_list)
newF189_list_hex = list(newF189_list_hex)
print(newF189_list_hex)
newF189_list_hex_join = "U,".join(newF189_list_hex) + "U,0x00U"
print(newF189_list_hex_join)

## <isrtval>
isrtval_filePath = rootPath + "\cpj_ca\ov_dia\cfg\ov_pdm_ov_dia_" + variant + ".isrtval"
with open(isrtval_filePath, 'r') as file:
    isrtval_list = file.readlines()

F189_indexes = [i for i, item in enumerate(isrtval_list) if re.search(oldF189_list_hex_join, item, re.IGNORECASE)]
F189_index = F189_indexes[0]
isrtval_oldF189line = isrtval_list[F189_index]
F189_patternIgCase = re.compile(re.escape(oldF189_list_hex_join), re.IGNORECASE)
isrtval_newF189line = F189_patternIgCase.sub(newF189_list_hex_join, isrtval_oldF189line)
isrtval_list[F189_index] = isrtval_newF189line

F189_indexes = [i for i, item in enumerate(isrtval_list) if re.search("<StructRef>Insert/ComponentConfigurations/ov_dia/Pdm/Structs/ov_dia_ov_diaRunnable_F189ECUSoftwareversionnumber</StructRef>", item, re.IGNORECASE)]
isrtval_oldF189version_line_index = F189_indexes[0] - 2
isrtval_oldF189version_line = isrtval_list[isrtval_oldF189version_line_index]
isrtval_oldF189version = re.findall(r'\d+', isrtval_oldF189version_line)
isrtval_oldF189version_line_new = isrtval_oldF189version_line.replace(isrtval_oldF189version[0], str(int(isrtval_oldF189version[0]) + 1))
isrtval_list[isrtval_oldF189version_line_index] = isrtval_oldF189version_line_new

with open(isrtval_filePath, 'w') as file:
    file.writelines(isrtval_list)
print("Made change to the .isrtval, line: ", F189_index)
print("Made change to the .isrtval, line: ", isrtval_oldF189version_line_index)
## </isrtval>

## <ov_diaRunnable_user.cpp>
RUserCpp_filePath = rootPath + "\cpj_ca\ov_dia\src\service_independent\\" + variant + "\ov_diaRunnable_user.cpp"
with open(RUserCpp_filePath, 'r') as file:
    RUserCpp_list = file.readlines()

RUserCpp_F189line = list(line for line in RUserCpp_list if "vfc::uint8_t ui_arr_TempF189[8]" in line)
RUserCpp_F189line_new = RUserCpp_F189line[0].replace(oldF189, newF189)
print RUserCpp_F189line[0] 
print RUserCpp_F189line_new 
RUserCpp_F189line_index = RUserCpp_list.index(RUserCpp_F189line[0])
RUserCpp_list[RUserCpp_F189line_index] = RUserCpp_F189line_new

with open(RUserCpp_filePath, 'w') as file:
    file.writelines(RUserCpp_list)
print("Made change to the ov_diaRunnable_user.cpp, line: ", RUserCpp_F189line_index)
## </ov_diaRunnable_user.cpp>

## <ov_dia_UserBswServiceTypes.hpp>
UBSTHpp_filePath = rootPath + "\cpj_ca\ov_pdm\inc\gen\\" + variant + "\ov_dia_UserBswServiceTypes.hpp"
with open(UBSTHpp_filePath, 'r') as file:
    UBSTHpp_list = file.readlines()

F189_indexes = [i for i, item in enumerate(UBSTHpp_list) if re.search(oldF189_list_hex_join, item, re.IGNORECASE)]

F189line_index_1 = F189_indexes[0]
UBSTHpp_oldF189line_1 = UBSTHpp_list[F189line_index_1]
UBSTHpp_oldF189line_1_patternIgCase = re.compile(re.escape(oldF189_list_hex_join), re.IGNORECASE)
UBSTHpp_newF189line_1 = UBSTHpp_oldF189line_1_patternIgCase.sub(newF189_list_hex_join, UBSTHpp_oldF189line_1)
UBSTHpp_list[F189line_index_1] = UBSTHpp_newF189line_1

F189line_index_2 = F189_indexes[1]
UBSTHpp_oldF189line_2 = UBSTHpp_list[F189line_index_2]
UBSTHpp_oldF189line_2_patternIgCase = re.compile(re.escape(oldF189_list_hex_join), re.IGNORECASE)
UBSTHpp_newF189line_2 = UBSTHpp_oldF189line_2_patternIgCase.sub(newF189_list_hex_join, UBSTHpp_oldF189line_2)
UBSTHpp_list[F189line_index_2] = UBSTHpp_newF189line_2

with open(UBSTHpp_filePath, 'w') as file:
    file.writelines(UBSTHpp_list)
print("Made change to the ov_diaRunnable_user.cpp, line: ", F189line_index_1)
print("Made change to the ov_diaRunnable_user.cpp, line: ", F189line_index_2)
## </ov_dia_UserBswServiceTypes.hpp>

## <ov_pdm_dataitem_config.cpp>
DICFGCpp_filePath = rootPath + "\cpj_ca\ov_pdm\inc\gen\\" + variant + "\ov_pdm_dataitem_config.cpp"
with open(DICFGCpp_filePath, 'r') as file:
    DICFGCpp_list = file.readlines()

F189_indexes = [i for i, item in enumerate(DICFGCpp_list) if re.search("NvMConf_NvMBlockDescriptor_NVM_ov_dia_ov_diaRunnable_F189ECUSoftwareversionnumber", item, re.IGNORECASE)]
F189_index = F189_indexes[0] + 2
DICFGCpp_oldF189version_line = DICFGCpp_list[F189_index]
DICFGCpp_oldF189version = re.findall(r'\d+', DICFGCpp_oldF189version_line)
DICFGCpp_oldF189version_line_new = DICFGCpp_oldF189version_line.replace(DICFGCpp_oldF189version[0], str(int(DICFGCpp_oldF189version[0]) + 1))
DICFGCpp_list[F189_index] = DICFGCpp_oldF189version_line_new

with open(DICFGCpp_filePath, 'w') as file:
    file.writelines(DICFGCpp_list)
print("Made change to the ov_pdm_dataitem_config.cpp, line: ", F189_index)
## </ov_pdm_dataitem_config.cpp>

## <ov_pdm_dataitem_handling.cpp>
DIHDLCpp_filePath = rootPath + "\cpj_ca\ov_pdm\inc\gen\\" + variant + "\ov_pdm_dataitem_handling.cpp"
with open(DIHDLCpp_filePath, 'r') as file:
    DIHDLCpp_list = file.readlines()

F189_indexes = [i for i, item in enumerate(DIHDLCpp_list) if re.search("NVM_CFG_NV_BLOCK_LENGTH_NVM_ov_dia_ov_diaRunnable_F189ECUSoftwareversionnumber - rbPdm::RBPDM_VERSION_WRITE_COUNTER_SIZE", item, re.IGNORECASE)]
F189_index = F189_indexes[0] + 2
DIHDLCpp_oldF189version_line = DIHDLCpp_list[F189_index]
DIHDLCpp_oldF189version = re.findall(r'\d+', DIHDLCpp_oldF189version_line)
strOldVersion = DIHDLCpp_oldF189version[2] + "U"
strNewVersion = str(int(DIHDLCpp_oldF189version[2]) + 1) + "U"
# Must be added "U" to prevent "F189" variable be changed, in case version = 1 | 8 | 9 | 18 | 89
DIHDLCpp_oldF189version_line_new = DIHDLCpp_oldF189version_line.replace(strOldVersion, strNewVersion)
DIHDLCpp_list[F189_index] = DIHDLCpp_oldF189version_line_new

with open(DIHDLCpp_filePath, 'w') as file:
    file.writelines(DIHDLCpp_list)
print("Made change to the ov_pdm_dataitem_handling.cpp, line: ", F189_index)
## </ov_pdm_dataitem_handling.cpp>

## <OverWriteGenFiles>
try:
    shutil.copy(UBSTHpp_filePath, rootPath + "\cpj_ca\ov_pdm\gen\ov_dia_UserBswServiceTypes.hpp")
    shutil.copy(DICFGCpp_filePath, rootPath + "\cpj_ca\ov_pdm\gen\ov_pdm_dataitem_config.cpp")
    shutil.copy(DIHDLCpp_filePath, rootPath + "\cpj_ca\ov_pdm\gen\ov_pdm_dataitem_handling.cpp")
    shutil.copy(DICFGCpp_filePath, rootPath + "\cpj_ca\\tools\InsertTool\PDM\PdmGen\cfg\ov_pdm_dataitem_config.cpp")
    print("File copied successfully.")
except shutil.SameFileError:
    print("Source and destination represents the same file.")
except PermissionError:
    print("Permission denied.")
except:
    print("Error occurred while copying file.")
## </OverWriteGenFiles>

raw_input("ENTER to exit...")
